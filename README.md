# NCCSerialized

A property wrapper that implements safe serialized access to a variable.
Use like this:

    @NCCSerialized var foo: String

    foo = "hello"
